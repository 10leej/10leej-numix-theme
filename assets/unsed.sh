#!/bin/sh
sed -i \
         -e 's/rgb(0%,0%,0%)/#222222/g' \
         -e 's/rgb(100%,100%,100%)/#bbbbbb/g' \
    -e 's/rgb(50%,0%,0%)/#222222/g' \
     -e 's/rgb(0%,50%,0%)/#ae3f3e/g' \
 -e 's/rgb(0%,50.196078%,0%)/#ae3f3e/g' \
     -e 's/rgb(50%,0%,50%)/#222222/g' \
 -e 's/rgb(50.196078%,0%,50.196078%)/#222222/g' \
     -e 's/rgb(0%,0%,50%)/#bbbbbb/g' \
	"$@"
